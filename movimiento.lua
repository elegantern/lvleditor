module(..., package.seeall)

local widget = require "widget"

local ctlHeigh = 300
local unidadBase = 15
local unidadInc = ctlHeigh/unidadBase -- velocidad de incremento, esta definida en 15 velocidades diferentes
local velocidad = 0
local unidadAcel = 22
local velocidadIzq = 0
local velocidadDer = 0
local stackTab = {}
stackTab[0] = 0
stackTab[1] = 0
finalizar = false
physicsOn = false

estatus = "parado" -- permite conocer en donde se encuentra el character
character = nil -- se utiliza para representar a oveja en la funcion movimiento
grp = nil       -- se utiliza para asignar 
itemMax = 3
itemCapturados = 0
txtScore = display.newText("Score 0 - 3", 0, 0, native.systemFont, 32)
txtFinish = nil

-- controls
ctlDerecha = nil
ctlIzq = nil

-- detiene la fisica y detiene las variables de movimiento representando el fin del juego
function gameFinish()
    acelMov = 0;
    finalizar = true
    
    if flechaMov ~=  nil then
       flechaMov:removeSelf( )
       flechaMov = nil
    end
   
    physics.pause()
end

function die()
    gameFinish()
    txtFinish = display.newText("Muerto madafaka!!", display.contentCenterX - 100, display.contentCenterY, native.systemFont, 48 )
end

function addItem()
    itemCapturados = itemCapturados + 1
    txtScore.text = "Score " .. itemCapturados .. " - " .. itemMax
end

collisionChar = function ( self, event )
     if physicsOn == false then
        return;
    end
    
    if ( event.phase == "began") then
        if event.other.name == "plataformaInicio" then
            estatus = "volando"
            local vx, vy = self:getLinearVelocity( )
            self:setLinearVelocity( vx, 0 )
            self:applyLinearImpulse( 0, -100, self.x, self.y )
        elseif event.other.name == "nube" then
            self.isSensor = true
            local vx, vy = self:getLinearVelocity( )
            self:setLinearVelocity( vx, 0 )
            self:applyLinearImpulse( 0, -100, self.x, self.y )
        elseif event.other.name == "item" then
            addItem()
            event.other.isSensor = true
            event.other:removeSelf( )
            event.other = nil
        elseif event.other.name == "final" then    
            self.isSensor = false
            gameFinish()
        end
    elseif event.phase == "ended" then
       self.isSensor = false
    end
end

function movimiento( event )
    if physicsOn == false then
        return;
    end
    
    if stackTab[0] == -1 then
        velocidad = velocidadIzq
    elseif stackTab[0] == 1 then
        velocidad = velocidadDer
    end
    
    local vx, vy = character:getLinearVelocity( )
    character:setLinearVelocity( velocidad , vy )
    
    if character ~= nil and character.y ~= nil and (character.y - character.contentHeight ) >= display.contentHeight then
        --die()
        character.y = character.y -1600
    end
    
end

function moveCamera() -- move camera function
    if physicsOn == false then
        return;
    end
    
    if character.x >= display.contentCenterX then  -- cuando la posicion del canguro es mayor que la mitad de la camara se mueve la camara
        grp.x = display.contentCenterX + character.x * -1
    end
    if character.y <= display.contentCenterY then
        grp.y = display.contentCenterY - character.y 
    end
end

local function addTouch( tab )
    if stackTab[0] ~= tab and stackTab[1] ~= tab then
        if stackTab[0] == 0 then
            stackTab[0] = tab
        else
            stackTab[1] = tab
        end
    end
    
end

local function delTouch( tab )
    if stackTab[0] == tab then
        if stackTab[1] ~= 0 then
            stackTab[0] = stackTab[1]
            stackTab[1] = 0
        else
            stackTab[0] = 0
        end
    elseif stackTab[1] == tab then
        stackTab[1] = 0
    end
end

local function drag()
    return true
end

local function releaseButton(param)
    local t = param.target
    display.getCurrentStage():setFocus( t, nil )
    t.focus = false
    delTouch(param.op)
end

local function movLeft( event )
    local t = event.target
    local distTmp = (event.x - event.target.x) / unidadInc
    velocidadIzq = (unidadBase - distTmp) * unidadAcel * -1

    if event.phase == "press" then
        display.getCurrentStage():setFocus( t, event.id )
        addTouch(-1)
    elseif event.phase == "moved" and ( event.y < t.y or event.y > t.contentHeight + t.y or  event.x < t.x or event.x > t.contentWidth + t.x ) then
        releaseButton( { target = t, op = -1 } )
    elseif event.phase == "release" then
        releaseButton( { target = t, op = -1 } )
    end
end

local function movRight( event )
    local t = event.target
    local distTmp = (event.x - event.target.x) / unidadInc
    velocidadDer = distTmp * unidadAcel
    
    if event.phase == "press" then
        display.getCurrentStage():setFocus( t, event.id )
        addTouch(1)
    elseif  event.phase == "moved" and ( event.y < t.y or event.y > t.contentHeight + t.y or  event.x < t.x or event.x > t.contentWidth + t.x ) then
        releaseButton( { target = t, op = 1 } )
    elseif event.phase == "release" then
        releaseButton( { target = t, op = 1 } )
    end
end

function inicializaControles()
    if physicsOn == false then
        return;
    end
    
    ctlDerecha = widget.newButton{
        defaultColor = { 251,236,107, 100 },
        left = display.contentWidth - 350, 
        top = display.contentHeight - 200,
        width=300, height=110,
        onEvent = movRight
    }
    
    ctlDerecha:toFront( )
    ctlDerecha:setReferencePoint( display.TopLeftReferencePoint )

    ctlIzq = widget.newButton{
        defaultColor = { 251,236,107, 100 },
        left = 50, 
        top = display.contentHeight - 200,
        width=300, height=110,
        onEvent = movLeft
    }
    
    ctlIzq:toFront()
    ctlIzq:setReferencePoint( display.TopLeftReferencePoint )
end

-- movimiento del oveja madafaka
Runtime:addEventListener( "enterFrame", movimiento )
Runtime:addEventListener( "enterFrame", moveCamera )