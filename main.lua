-- Hide status bar
display.setStatusBar(display.HiddenStatusBar)

-- Physics Engine
local physics = require "physics"
physics.start()
physics.setGravity(0,15)

system.activate( "multitouch" )

-- se puede usar movimiento
local mov = require "movimiento"

-- se usa loader
local cargar = require "cargador"

--Declaracion de los grupos de objetos
local Botones = nil
group = display.newGroup()
mov.grp = group

vx, vy = 0,0

local tablaObjetos={}
local objetoActual

-- Main Function
function main()
lvleditor()
end

function lvleditor()
    Botones = display.newGroup( )
    
    -- Creación del fondo
    fondo = display.newImage( "images/fondo.png",0,0 )
    
    --Creación de los botones
    btnNube = display.newImage ( "images/btnnube.png",0,735)
    btnNube.name = "nube"
    btnOveja = display.newImage ( "images/btnoveja.png",0,700)
    btnOveja.name = "oveja"
    btnTrampa = display.newImage ( "images/btntrampa.png",96,735)
    btnTrampa.name = "trampa"
    btnItem = display.newImage ( "images/btnitem.png",192,735)
    btnItem.name = "item"
    btnAngulo = display.newImage ( "images/btnangulo.png",288,735)
    btnAngulo.name = "angulo"
    btnIncTamX = display.newImage ( "images/btnincx.png",384,735)
    btnIncTamX.name = "inctamx"
    btnIncTamY = display.newImage ( "images/btnincy.png",480,735)
    btnIncTamY.name = "inctamy"
    btnDecTamX = display.newImage ( "images/btndecx.png",576,735)
    btnDecTamX.name = "dectamx"
    btnDecTamY = display.newImage ( "images/btndecy.png",672,735)
    btnDecTamY.name = "dectamy"
    btnGuardarEsc = display.newImage ( "images/btnguardar.png",768,735)
    btnGuardarEsc.name = "save"
    btnPhysicsOff = display.newImage ( "images/btnphysicsoff.png",864,720)
    btnPhysicsOff.name = "physicsoff"
    btnPhysicsOn = display.newImage ( "images/btnphysicson.png",864,670)
    btnPhysicsOn.name = "physicson"
    btnImprimir = display.newImage( "images/btnimprimir.png",672,700)
    btnImprimir.name = "imprimir"
    btnBorrar = display.newImage( "images/btnborrar.png",768,700)
    btnBorrar.name = "borrar"
    
    rightCam = display.newImage( "images/flechader.png")
    rightCam:setReferencePoint(display.TopLeftReferencePoint )
    rightCam.x = 110;rightCam.y = 80
    rightCam.name = "right"
    rightCam.alpha = 0.3
    
    leftCam = display.newImage("images/flechaizq.png")
    leftCam:setReferencePoint(display.TopLeftReferencePoint )
    leftCam.x = -10;leftCam.y = 80
    leftCam.name = "left"
    leftCam.alpha = 0.3
    
    upCam = display.newImage("images/flechaup.png")
    upCam:setReferencePoint(display.TopLeftReferencePoint )
    upCam.x = 60;upCam.y = 10
    upCam.name = "up"
    upCam.alpha = 0.3
    
    downCam = display.newImage("images/flechadown.png",460,565)
    downCam:setReferencePoint(display.TopLeftReferencePoint )
    downCam.x = 60;downCam.y = 120
    downCam.name = "down"
    downCam.alpha = 0.3
    
    --se insertan los objetos en los grupos
    Botones:insert(rightCam)
    Botones:insert(leftCam)
    Botones:insert(upCam)
    Botones:insert(downCam)
    Botones:insert(btnNube)
    Botones:insert(btnTrampa)
    Botones:insert(btnItem)
    Botones:insert(btnOveja)
    Botones:insert(btnAngulo)
    Botones:insert(btnIncTamX)
    Botones:insert(btnIncTamY)
    Botones:insert(btnDecTamX)
    Botones:insert(btnDecTamY)
    Botones:insert(btnGuardarEsc)
    Botones:insert(btnPhysicsOff)
    Botones:insert(btnPhysicsOn)
    Botones:insert(btnImprimir)  
    Botones:insert(btnBorrar)     
    group:insert(fondo)
    
    --se mandan los objetos del grupo al frente
    Botones:toFront()
    
    --listeners de los botones
    rightCam:addEventListener( "tap", accionFlechas)
    leftCam:addEventListener( "tap", accionFlechas)
    upCam:addEventListener( "tap", accionFlechas)
    downCam:addEventListener( "tap", accionFlechas)
    btnOveja:addEventListener( "tap", creaOveja)
    btnNube:addEventListener( "tap", creaObjeto)
    btnTrampa:addEventListener( "tap", creaObjeto)
    btnItem:addEventListener( "tap", creaObjeto)
    btnAngulo:addEventListener( "tap", modificarObjeto)
    btnIncTamX:addEventListener( "tap", modificarObjeto)
    btnIncTamY:addEventListener( "tap", modificarObjeto)
    btnDecTamX:addEventListener( "tap", modificarObjeto)
    btnDecTamY:addEventListener( "tap", modificarObjeto )
    btnImprimir:addEventListener( "tap", imprimirTabla )
    btnPhysicsOn:addEventListener( "tap", physicsOn )
    btnPhysicsOff:addEventListener( "tap", physicsOff )
    btnBorrar:addEventListener( "tap", borrarObjeto )
    btnGuardarEsc:addEventListener( "tap", guardargroup )
end

function seleccionarObjeto (event)
    if event.phase == "began" then
        objetoActual = event.target
        if event.target.name == "nube" then 
            objetoActual:setFillColor( 0, 255, 0)
        elseif event.target.name == "trampa" then
            objetoActual:setFillColor( 0, 255, 0)
        end
    elseif event.phase == "moved" then
        objetoActual.x = event.x+(group.x*-1) 
        objetoActual.y = event.y+(group.y*-1)
    elseif event.phase == "ended" then
        if event.target.name == "nube" then 
            objetoActual:setFillColor( 255,255, 255 )
        elseif event.target.name == "trampa" then 
            objetoActual:setFillColor( 220, 0, 20 )
        end
    end
    return true
    
end

function creaOveja(event)
    if event.target.name == "oveja" then
        oveja = display.newImage( "images/sheep.png",((group.x*-1+display.contentWidth/2)-50),(group.y*-1+ display.contentHeight/2-60 ))
        physics.addBody( oveja, "static", { density=1.0, friction=0.3, bounce=0 } )
        mov.character = oveja
        group:insert( oveja )
    end
    oveja:addEventListener( "touch", seleccionarObjeto )
end

function accionFlechas(event)
    --camDerecha
    if event.target.name == "right" then
        group.x = group.x -250          
    --camIzquierda
    elseif event.target.name == "left" then
        group.x = group.x + 250
    --camArriba
    elseif event.target.name == "up" then
        group.y = group.y + 250
    --camAbajo
    elseif event.target.name == "down" then
        group.y = group.y - 250   
    end
    
end

function creaObjeto(event)
    
    local objeto = nil
    
    --creanube
    if event.target.name == "nube" then
        objeto = display.newImage("images/nube.png",((group.x*-1+display.contentWidth/2)-50),(group.y*-1+ display.contentHeight/2-60 ))
        objeto.name = "nube"
        physics.addBody( objeto, "static", { friction=0.3 } )
        group:insert(objeto)
        --creaTrampa    
    elseif event.target.name == "trampa" then
        objeto = display.newRect( ((group.x*-1+display.contentWidth/2)-50),(group.y*-1+ display.contentHeight/2-60 ), 80, 220 )
        objeto.name = "trampa"
        physics.addBody( objeto, "static")
        objeto:setFillColor( 220,0,20 )
        group:insert(objeto)
        --creaitem
    elseif event.target.name == "item" then
        objeto = display.newImage( "images/item1.png",((group.x*-1+display.contentWidth/2)-50),(group.y*-1+ display.contentHeight/2-60 ) )
        objeto.name = "item"
        physics.addBody( objeto, "static", { friction=0 } )
        group:insert(objeto)
    end
    
    --se agrega el objeto a la tabla    
    tablaObjetos[table.getn(tablaObjetos)+1]=objeto
    
    --listener del objeto para poder seleccionarlo y moverlo
    objeto:addEventListener( "touch", seleccionarObjeto )
    
    -- el objeto creado es igual al objeto actual
    objetoActual = objeto 
    
end

function modificarObjeto(event)
    if objetoActual ~= oveja and objetoActual.name ~= "item" then    
        if event.target.name == "angulo" then
            objetoActual.rotation = objetoActual.rotation+5
        elseif event.target.name == "inctamx" then
            objetoActual.width = objetoActual.width+5
        elseif event.target.name == "inctamy" then
            objetoActual.height = objetoActual.height+10
        elseif event.target.name == "dectamx" then
            objetoActual.width = objetoActual.width-5
        elseif event.target.name == "dectamy" then
            objetoActual.height = objetoActual.height-10
        end
    end
end

function imprimirTabla(event)
    for i=1,#tablaObjetos do
        saveData = ""
        local objeto,name,body,color,rotation,correccionxy,grupo;
        local numObjetos
        numObjetos = table.getn(tablaObjetos)
        for i=1,numObjetos do
            if tablaObjetos[i].name == "nube" then
                if tablaObjetos[i].x then
                    objeto = 'objeto = display.newImage("images/nube.png"' .. "," .. tablaObjetos[i].width .. "," .. tablaObjetos[i].height .. ")" .. ";"
                    name = 'objeto.name = "nube"' .. ";"
                    x = 'objeto.x = ' .. tablaObjetos[i].x .. ";"
                    y = 'objeto.y = ' .. tablaObjetos[i].y .. ";"
                    body = 'physics.addBody( objeto, "static", { friction=0.3 } )' .. ";"
                    grupo = 'group:insert (objeto)' .. ";"
                    saveData = saveData .. objeto
                    saveData = saveData .. name
                    saveData = saveData .. x
                    saveData = saveData .. y
                    saveData = saveData .. body
                    saveData = saveData .. grupo
                    saveData = saveData .. "\n"
                end
            elseif tablaObjetos[i].name == "trampa" then
                if tablaObjetos[i].x then
                    objeto = 'objeto = display.newRect(' .. tablaObjetos[i].x .. "," .. tablaObjetos[i].y .. "," ..  tablaObjetos[i].width .. "," .. tablaObjetos[i].height .. ")" .. ";"
                    name = 'objeto.name = "trampa"' .. ";"
                    body = 'physics.addBody( objeto, "static")' .. ";"
                    color = 'objeto:setFillColor( 220,0,20 )' .. ";"
                    rotation = "objeto:rotate (" .. tablaObjetos[i].rotation .. ')' .. ";"
                    correccionxy = "objeto.x,objeto.y ="..tablaObjetos[i].x .. "," .. tablaObjetos[i].y .. ";"
                    grupo = 'group:insert (objeto)' .. ";"
                    saveData = saveData .. objeto
                    saveData = saveData .. name
                    saveData = saveData .. body
                    saveData = saveData .. color
                    saveData = saveData .. rotation
                    saveData = saveData .. correccionxy
                    saveData = saveData .. grupo
                    saveData = saveData .. "\n"
                end
            elseif tablaObjetos[i].name == "item" then
                if tablaObjetos[i].x then
                    objeto = 'objeto = display.newImage("images/item1.png"' .. "," .. tablaObjetos[i].width .. "," .. tablaObjetos[i].height .. ")" .. ";"
                    name = 'objeto.name = "item"' .. ";"
                    x = 'objeto.x = ' .. tablaObjetos[i].x .. ";"
                    y = 'objeto.y = ' .. tablaObjetos[i].y .. ";"
                    body = 'physics.addBody( objeto, "static", { friction=0 } )' .. ";"
                    grupo = 'group:insert (objeto)' .. ";"
                    saveData = saveData .. objeto
                    saveData = saveData .. name
                    saveData = saveData .. x
                    saveData = saveData .. y
                    saveData = saveData .. body
                    saveData = saveData .. grupo
                    saveData = saveData .. "\n"
                end
            end
        end
        
        local path = system.pathForFile( nil, system.ResourceDirectory)
        path = path .. "/levels/level1c.txt"
        local file = io.open( path , "w" )  
        file:write(saveData)
        io.close(file)
        file = nil
    end
end

function physicsOn(event)
    if oveja then
        mov.physicsOn = true
        mov.inicializaControles()
        
        oveja.bodyType = "dynamic"
        oveja.isFixedRotation = true -- las fuerzas que interactuen con el no haran que se mueva de su vertical
        oveja.isBullet = true
        
        oveja.collision = mov.collisionChar
        oveja:addEventListener( "collision", oveja )
    end
end

function physicsOff(event)
    if event.target.name == "physicsoff" then
        if mov.physicsOn then
            mov.physicsOn = false
            mov.ctlDerecha.isVisible = false
            mov.ctlIzq.isVisible = false
            oveja.bodyType = "static"
        end
    end
end

function borrarObjeto(event)
    if objetoActual ~= nil then      
        if event.target.name == "borrar" then
            -- table.remove ( tablaObjetos , objetoActual ) esto es lo que debe hacer,ocupo saber la posicion de objetoActual en la tabla.
            local index
            for i = 1,table.getn(tablaObjetos) do
                if tablaObjetos[i].x == objetoActual.x and tablaObjetos[i].y == objetoActual.y then
                    index = i
                end
            end
            table.remove(tablaObjetos,index)
            objetoActual:removeSelf( )
            objetoActual = nil
        end
    end
end

function guardargroup(event)
    if event.target.name == "save" then
        saveData = ""
        local numObjetos
        numObjetos = table.getn(tablaObjetos)
        print(numObjetos)
        for i=1,numObjetos do
            if tablaObjetos[i].x then
                saveData = saveData .. tablaObjetos[i].name
                saveData = saveData .. "," .. tablaObjetos[i].x
                saveData = saveData .. "," .. tablaObjetos[i].y
                saveData = saveData .. "," ..  tablaObjetos[i].width
                saveData = saveData .. "," ..  tablaObjetos[i].height
                saveData = saveData .. "," ..  tablaObjetos[i].rotation
                saveData = saveData .. "\n"
            end
            print(saveData)
        end
        
        local path = system.pathForFile( "level1.txt", system.DocumentsDirectory)
        local file = io.open( path , "w" )  
        if file then
            file:write(saveData)
            io.close(file)
        end
        display.newText("archivo guardado", display.contentWidth - 200, 100, native.systemFont, 32 )
    end
end

main()

local objectsStored = cargar.getObjects("level1.txt",seleccionarObjeto)
if #objectsStored > 0 then
for i=1,#objectsStored do
    local obj = objectsStored[i]
    local numObjetos
    numObjetos = table.getn(tablaObjetos)
    tablaObjetos[numObjetos+1]= obj
    obj:addEventListener( "touch", seleccionarObjeto )
    group:insert(obj)
    end
end
     
local baseInicio = display.newRect( 0, display.contentHeight - 20 , 400, display.contentHeight)
local baseFinal =  display.newImage("images/finish.png",22000,display.contentHeight - 100)  
local plataforma = display.newRect( 400, display.contentHeight - 20 , 50, display.contentHeight)
physics.addBody( baseInicio, "static", { friction=0.3 } )
physics.addBody( baseFinal, "static", { friction=0.3 } )
physics.addBody( plataforma, "static", { friction=0.3 } )
baseInicio.name = "inicio"
baseFinal.name = "final"
plataforma.name = "plataformaInicio"
baseInicio:setFillColor( 96,109,128 )
plataforma:setFillColor( 43,76,126 )
group:insert( baseInicio )
group:insert( baseFinal )
group:insert( plataforma)