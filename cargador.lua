module(..., package.seeall)

function Split(str, delim, maxNb)
    if string.find(str, delim) == nil then
        return { str }
    end
    if maxNb == nil or maxNb < 1 then
        maxNb = 0    -- No limit
    end
    local result = {}
    local pat = "(.-)" .. delim .. "()"
    local nb = 0
    local lastPos
    for part, pos in string.gfind(str, pat) do
        nb = nb + 1
        result[nb] = part
        lastPos = pos
        if nb == maxNb then break end
    end
        -- Handle the last field
        if nb ~= maxNb then
            result[nb + 1] = string.sub(str, lastPos)
        end
        return result
    end
    
    
    function getObjects(nombreArchivo,listener)
        
        local tablaObjetosEscenario={}
        objeto = nil
        
        local path = system.pathForFile( nombreArchivo, system.DocumentsDirectory)
        
        local file = io.open( path, "r" )
        if file then
            for line in file:lines() do
                palabras = {}
                palabras = Split(line,",")
                -- hay que separar las lineas por palabras, despues asignarle a cada palabra una variable(nombre = objeto.name), y ya con las variables crear el codigo para los objetos
                nombre = palabras[1]
                x = palabras[2]
                y = palabras[3]
                width = palabras[4]
                height = palabras[5]
                angulo = palabras[6]
                
                if nombre == "nube" then
                    objeto = display.newImage("images/nube.png",(width),(height))
                    objeto.name = "nube"
                    objeto.x = x ; objeto.y = y
                    physics.addBody( objeto, "static", { friction=0.3 } )
                elseif nombre == "trampa" then
                    objeto = display.newRect( x, y, width, height )
                    objeto:setReferencePoint( centerReferencePoint )
                    objeto.name = "trampa"
                    physics.addBody( objeto, "static")
                    objeto:setFillColor( 220,0,20 )
                    objeto:rotate ( palabras[6])
                    objeto.x , objeto.y =x,y;
                elseif nombre == "item" then
                    objeto = display.newImage( "images/item1.png",width,height )
                    objeto.name = "item"
                    objeto.x = x ; objeto.y = y
                    physics.addBody( objeto, "static", { friction=0 } )
                end
                tablaObjetosEscenario[table.getn(tablaObjetosEscenario)+1]=objeto
            end
            
            io.close( file )
            return tablaObjetosEscenario
        end
        
end
